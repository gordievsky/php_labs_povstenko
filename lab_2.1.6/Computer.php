<?php

class Computer {
	private $Name = '';
	private $CPU = '';
	private $RAM = '';
	private $Video = '';
	private $HDD = '';
	private $IsDesktop = true;
	private $Status = 'OFF';
	private $Desc = 'Computer : Identify by login and password';

  //Setters
  function setName($name) {
    $this->Name = ucwords(strtolower($name));
  }
  function setCPU($cpu) {
    $this->CPU = ucwords(strtolower($cpu));
  }
  function setRAM($ram) {
    $this->RAM = ucwords(strtolower($ram));
  }
  function setVideo($video) {
    $this->Video = ucwords(strtolower($video));
  }
  function setHDD($hdd) {
    $this->HDD = ucwords(strtolower($hdd));
  }
  function setDesc($desc) {
    $this->Desc = ucwords(strtolower($desc));
  }

  //Getters
  function getName() {
    return $this->Name;
  }
  function getCPU() {
    return $this->CPU;
  }
  function getRAM() {
    return $this->RAM;
  }
  function getVideo() {
    return $this->Video;
  }
  function getHDD() {
    return $this->HDD;
  }
  function getDesc() {
    return $this->Desc;
  }
  function getStatus() {
    return $this->Status;
  }
  

  function outputData() {
		return "<i>{$this->Status}</i> || <b>{$this->Name}: </b> <b>CPU: </b>{$this->CPU}; <b>RAM: </b> {$this->RAM}; <b>Video: </b> {$this->Video}; <b>Memory: </b> {$this->HDD}; is desktop: {$this->IsDesktop};<br/>";
  }
  
	function printParameters() {
		$status = $this->Status;
		//var_dump($status);
		if($status == 'ON') {
			return $this->outputData();
		} else {
			 echo ( '<i style="color: red"> ERROR, Computer is OFF !</i> ' );
		}
	}

	function identifyUser($desc) {
		$desc = $this->Desc;
		echo($desc);
	}

	function start() {
		$this->Status = 'ON';
	}
	function shutdown() {
		$this->Status = 'OFF';
	}
	function restart() {
		$this->Status = 'Restarting';
	}

}

?>