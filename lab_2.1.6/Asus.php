<?php

class Asus extends Computer {}

$asus = new Asus();
$asus->setName('My Asus');
$asus->setCPU('Core i5 - 6600');

$asus->start();

echo( $asus->getName() . ' is ' . $asus->getStatus() . '<br>' );
echo( $asus->printParameters() . '<br><hr>' );
?>