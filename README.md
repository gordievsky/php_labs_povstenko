# PHP_Labs_Povstenko

•Student should create class Console
•Student should create four static properties into Console class:
    o$success – with string “SUCCESS”
    o$failure – with string “FAILURE”
    o$warning – with string “WARNING”
    o$note – with string “NOTE”

Student should create static method printLine($message, $status = ‘’) into class Console
	Parameter $message will accept string to print
	Parameter $status will accept value of any Console class attribute
	
	Method printLine() must print message in one of four colors depending on the $status parameter and white by default. List of colors and output string format student can see by this link: http://softkube.com/blog/generating-command-line-colors-with-php
	
•Student should change all direct strings printing to calling Console class’ method printLine() with correct parameters