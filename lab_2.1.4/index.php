<?php 

require_once 'Asus.php';
require_once 'MacBook.php';
require_once 'Lenovo.php';

class Computer {
	public $Name = '';
	public $CPU = '';
	public $RAM = '';
	public $Video = '';
	public $HDD = '';
	public $IsDesktop = true;
	public $Status = 'OFF';
	public $Desc = 'Computer : Identify by login and password';

	//DRY
	public function outputData() {
		return "<i>{$this->Status}</i> || <b>{$this->Name}: </b> <b>CPU: </b>{$this->CPU}; <b>RAM: </b> {$this->RAM}; <b>Video: </b> {$this->Video}; <b>Memory: </b> {$this->HDD}; is desktop: {$this->IsDesktop};<br/>";
	}
	public function printParameters() {
		$status = $this->Status;
		//var_dump($status);
		if($status == 'ON') {
			return $this->outputData();
		} else {
			 echo ( '<i style="color: red"> ERROR, Computer is OFF !</i> ' );
		}
	}

	public function identifyUser($desc) {
		$desc = $this->Desc;
		echo($desc);
	}

	function start() {
		$this->Status = 'ON';
	}
	function shutdown() {
		$this->Status = 'OFF';
	}
	function restart() {
		$this->Status = 'Restarting';
	}

}

/*
$my_computer = new Computer();
$my_computer->Name = 'My home PC';
$my_computer->CPU = 'Intel Core i5 - 4460, 3.2 GHz';
$my_computer->RAM = 'DDR3, 8 Gb';
$my_computer->Video = 'Intel HD Graphics 4600';
$my_computer->HDD = 'HDD 500 Gb';

$my_notebook = new Computer();
$my_notebook->Name = 'My notebook';
$my_notebook->CPU = 'Intel Core i7 - 6600, 3.2 GHz';
$my_notebook->RAM = 'DDR3, 8 Gb';
$my_notebook->Video = 'GeForce 920MX';
$my_notebook->HDD = 'SSD 500 Gb';
$my_notebook->IsDesktop = 0;

$my_computers[] = $my_computer;
$my_computers[] = $my_notebook;

function setTimeout($fn, $timeout) {
	sleep(($timeout/1000));
	$fn();
}
*/
/*
$my_computer->restart();

echo "<h2>My Computers</h2>";
for($i=0; $i < count($my_computers); $i++) {
	$numb = $i+1 . ") ";
	$my_computers[$i]->IsDesktop = ($my_computers[$i]->IsDesktop) ? 'YES' : 'NO';
	if($my_computers[$i] instanceof Computer) {
		echo $numb . $my_computers[$i]->outputData();
	} else echo 'This is Not your Computer';
}*/
//var_dump($my_computers[0]);

?>