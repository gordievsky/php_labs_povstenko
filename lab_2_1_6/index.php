<?php 

require_once 'Computer.php';
require_once 'Asus.php';
require_once 'MacBook.php';
require_once 'Lenovo.php';

class Console {
		
	public static $success = 'SUCCESS';
	public static $failure = 'FAILURE';
	public static $warning = 'WARNING';
	public static $note = 'NOTE';

	public static function printLine($message, $status = '') {

		$message = '';
		$status = $this::$status;
	}
}
var_dump(Console::$warning);

/*
class ParentClass {
	public static $querty = 'parent var';
}

class ChildClass extends ParentClass {
	public static $test = 'child var';
	public function doSomething() {
		var_dump(self::$test, parent::$querty);
	}
}
(new ChildClass())->doSomething();
*/

/*
class Product {

	private static $num_instances_created = 0;

	public function __construct () {

			self::$num_instances_created++;

			echo "I've created " . self::$num_instances_created . " instances so far!<br/>\n";

	}

}

$product = new Product();
$productTest = new Product();
*/

?>