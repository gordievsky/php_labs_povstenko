<?php

class Lenovo extends Computer {}

$lenovo = new Lenovo();
$lenovo->Name = 'My Lenovo';
$lenovo->Desc = 'Lenovo: Identify by fingerprints';

$lenovo->start();

echo( $lenovo->Name . ' is ' . $lenovo->Status . '<br>' );
echo( $lenovo->identifyUser($lenovo) . '<br>' . $lenovo->printParameters() . '<br><hr>' );

?>