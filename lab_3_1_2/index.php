<?php

$request = $_SERVER['REQUEST_URI'];
$arrMoved = [
	"index.php" => "index.php",
	"home.php/" => "home.php/",
    "content.php" => "content.php"
];
           
if(array_key_exists($request, $arrMoved)) {
  $newplace="http://".$_SERVER['HTTP_HOST'].$arrMoved[$request];
  header("HTTP/1.0 301 Moved Permanently");
  header("Location: $newplace");
  header("Connection: close");
  exit(); // ALWAYS use exit() after header()
}
else {
  header("HTTP/1.0 404 Not Found");
}

?>