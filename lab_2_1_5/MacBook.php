<?php

class MacBook extends Computer {}

$macbook = new MacBook();
$macbook->setName('My MacBook');
$macbook->setDesc('MacBook: Identify by Apple ID');

$macbook->shutdown();

echo( $macbook->getName() . ' is ' . $macbook->getStatus() . '<br>' );
echo( $macbook->identifyUser($macbook) . '<br>' . $macbook->printParameters() . '<br><hr>' );

?>