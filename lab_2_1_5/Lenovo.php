<?php

class Lenovo extends Computer {}

$lenovo = new Lenovo();
$lenovo->setName('My Lenovo');
$lenovo->setDesc('Lenovo: Identify by fingerprints');

$lenovo->start();

echo( $lenovo->getName() . ' is ' . $lenovo->getStatus() . '<br>' );
echo( $lenovo->identifyUser($lenovo) . '<br>' . $lenovo->printParameters() . '<br><hr>' );

?>